#include <iostream>
#include <string>
#include "cipher.hpp"

using namespace std;

#ifndef DATECIPHER_HPP_
#define DATECIPHER_HPP_

class DateCipher:public Cipher {
public:
    string encrypt(string &text);
    string decrypt(string &text);

    int getKey() {return key;}
    void setKey(int num) {key = num;}

private:
    int key;
};

#endif
