#include <iostream>
#include <string>
#include "caesar.cpp"

using namespace std;

int main() {
    CaesarCipher cipher;

    // Ask user for key.
    int num;
    cout << "Enter integer value for key: ";
    cin >> num;
    cipher.setKey(num);

    // Ask user for text to encrypt.
    string text;
    cout << "Enter text to encrypt: ";
    cin.ignore();
    getline(cin, text);

    string encrypt_text = cipher.encrypt(text);
    cout << "Encrypted text: " << encrypt_text << endl;

    string decrypt_text = cipher.decrypt(encrypt_text);
    cout << "Decrypted text: " << decrypt_text << endl;
}
