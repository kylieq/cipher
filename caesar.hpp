#include <iostream>
#include <string>
#include "cipher.hpp"

using namespace std;

#ifndef CAESARCIPHER_HPP_
#define CAESARCIPHER_HPP_

class CaesarCipher: public Cipher {
public:
    string encrypt(string &text);
    string decrypt(string &text);

    int getKey() {return key;}
    void setKey(int num) {key = num;}

private:
    int key;
};

#endif
