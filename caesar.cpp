#include <iostream>
#include <string>
#include "caesar.hpp"

using namespace std;

string CaesarCipher::encrypt(string &text) {
    int idx;
    string letters = "abcdefghijklmnopqrstuvwxyz ";
    for (int i=0; i<text.size(); i++) {
        idx = letters.find(text[i]);
        int total = key + idx;
        int new_idx;
        if (total > 26) {
            int difference = total - 26;
            new_idx = -1 + difference;
        }
        else {
            new_idx = total;
        }
        text[i] = letters[new_idx];
    }
    return text;
}

string CaesarCipher::decrypt(string &text) {
    int idx;
    string letters = "abcdefghijklmnopqrstuvwxyz ";
    for (int i=0; i<text.size(); i++) {
        idx = letters.find(text[i]);
        int total = idx - key;
        int new_idx;
        if (total < 0) {
            int difference = 27 + total;
            new_idx = difference;
        }
        else {
            new_idx = total;
        }
        text[i] = letters[new_idx];
    }
    return text;
}
