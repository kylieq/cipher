#include <iostream>
#include <sstream>
#include <string.h>
#include "date.cpp"

using namespace std;

int main() {
    DateCipher cipher;

    string months[12] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    string date;
    cout << "Enter date: ";
    getline(cin, date);
    cout << "Date selected: " << date << endl;

    string empty[] = {" ", ","};

    // Extract month from date entered.
    int pos1 = date.find(empty[0]);
    string month = date.substr(0, pos1);
    month[0] = toupper(month[0]);
    int month_key;
    for (int i=0; i<12; i++) {
        if (months[i] == month) {
            month_key = i+1;
        }
    }
    month = to_string(month_key);

    // Extract day from date entered.
    int pos2 = date.find(empty[1]);
    string day = date.substr(pos1+1, 2);
    char comma = ',';
    string zero = "0";
    if (day[1] == comma) {
        day = zero + day[0];
    }

    // Extract year from date entered.
    int pos3 = date.find(empty[0], pos1+1);
    string year = date.substr(pos3+3, 2);

    // Create key from date entered.
    int key;
    string keyStr = month + day + year;
    cout << key << endl;
    stringstream ss(keyStr);
    ss >> key;
    cipher.setKey(key);

    cout << "Date selected: " << month << "/" << day << "/" << year << endl;
    cout << "Key: " << key << endl;

    string text;
    cout << "Enter text to encrypt: ";
    getline(cin, text); 
    
    cout << "\nText entered:   " << text << endl;
    string encrypt_str = cipher.encrypt(text);
    
    string decrypt_str = cipher.decrypt(encrypt_str);
}   

