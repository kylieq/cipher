#include <iostream>
#include <sstream>
#include <string.h>
#include "date.hpp"

using namespace std;

string DateCipher::encrypt(string &text) {
    string letters = "abcdefghijklmnopqrstuvwxyz";
    string key_str = to_string(key);
    string new_text;
    int ctr = 0;

    // Change characters in text to their corresponding digit from key.
    for (int i=0; i<text.size(); i++) {
        if (text[i] == ' ') {
            new_text += text[i];
        }
        else {
            if (ctr < key_str.size()) {
                new_text += key_str[ctr];
            }
            else {
                ctr = 0;
                new_text += key_str[ctr];
            }
            ctr += 1;
        }
    }

    cout << "Enrypted text:  " << new_text << endl;

    // Shift letters depending on their corresponding digit from key.
    int idx;
    for (int i=0; i<new_text.size(); i++) {
        idx = letters.find(text[i]);
        int num = (int) new_text[i] - 48;
        int total = idx + num;
        if (new_text[i] == ' ') {
            continue;
        }
        else if (total > 25) {
            idx = total - 26;
        }
        else {
            idx = total;
        }
        new_text[i] = letters[idx];
    }

    cout << "Enrypted text:  " << new_text << endl;
    return new_text;
}

string DateCipher::decrypt(string &text) {
    string letters = "abcdefghijklmnopqrstuvwxyz";
    string key_str = to_string(key);
    string new_text;
    int idx, key_digit, total;
    int ctr = 0;

    for (int i=0; i<text.size(); i++) {
        if (ctr >= key_str.size()) {
            ctr = 0;
        }

        idx = letters.find(text[i]);
        key_digit = (int) key_str[ctr] - 48;
        total = idx - key_digit;

        if (text[i] == ' ') {
           new_text += ' ';
           continue;
        }
        else if (total < 0) {
            idx = 26 + total;
        }
        else {
            idx = total;
        }
        new_text += letters[idx];
        ctr += 1;
    }

    cout << "Decrypted text: " << new_text << endl;
    return new_text;
}
